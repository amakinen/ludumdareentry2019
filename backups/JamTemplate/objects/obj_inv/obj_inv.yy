{
    "id": "895f1943-224a-471a-9f98-a0c7d9f1c3df",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_inv",
    "eventList": [
        {
            "id": "26f736e5-c3e2-4c36-bd3f-789749baab79",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "895f1943-224a-471a-9f98-a0c7d9f1c3df"
        },
        {
            "id": "1f66286e-2fd8-4539-b237-762266b49727",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "895f1943-224a-471a-9f98-a0c7d9f1c3df"
        },
        {
            "id": "dd7d53d3-4592-45e1-bf8e-8405aebe38d2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "895f1943-224a-471a-9f98-a0c7d9f1c3df"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}