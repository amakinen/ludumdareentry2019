    if (live_call()) return live_result;
    
    
    index_offset *= 0.7;
    
    draw_set_color(c_black);
    
    
    var _len = array_length_1d(item_list);
    for(var i = 0; i < _len; ++i) {
        var _item = item_list[i];
        var _item_index_offset = item_index - i + index_offset;
        var _item_y = _item_index_offset * 100;
        var _alpha = 1-abs(item_index + index_offset - i)/2;
            _alpha = clamp(_alpha, 0, 1);
        draw_set_alpha(_alpha);
        draw_text(room_width-75, room_height/2-_item_y, _item);
        draw_set_alpha(1);
    }
    
    /*var item_target_y = room_height/2 + (item_index) * 75;
    
    for(var i = 0; i < array_length_1d(item_list); ++i) {
            item_y += (item_target_y - item_y)  * 0.01;
            var yoff = 75 * (i-item_index);
            draw_text(room_width-75, item_y + yoff, item_list[i])
            //draw_rectangle(room_width-75, floor(item_y+yoff-25), room_width-25, floor(item_y+yoff+25), false);
    }
    
    
    if keyboard_check_pressed(ord("R")) room_restart();