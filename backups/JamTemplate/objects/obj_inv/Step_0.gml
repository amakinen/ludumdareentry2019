/// @description Insert description here
    if (live_call()) return live_result;
    
    var _index_orig = item_index;
    
    var _index_dir = keyboard_check_pressed(vk_down) - keyboard_check_pressed(vk_up);
    if (_index_dir != 0) {
        if clamp(item_index + _index_dir, 0, array_length_1d(item_list)-1) != item_index {
            item_index += _index_dir;
            index_offset = -_index_dir;
        }
    }