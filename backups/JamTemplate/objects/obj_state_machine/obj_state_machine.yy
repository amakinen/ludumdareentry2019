{
    "id": "a2b7cee7-2cec-48c4-90af-933e5e9fb6a8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_state_machine",
    "eventList": [
        {
            "id": "f5a6e243-26fe-5378-83b3-c0eeb9aa912f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a2b7cee7-2cec-48c4-90af-933e5e9fb6a8"
        },
        {
            "id": "390ecb16-cae0-0842-36c2-18fd1f9eaf5f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "a2b7cee7-2cec-48c4-90af-933e5e9fb6a8"
        },
        {
            "id": "4e815180-90b2-f3f2-5632-8f0049f6f98f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "a2b7cee7-2cec-48c4-90af-933e5e9fb6a8"
        },
        {
            "id": "0edd9a4c-c0fb-4688-887e-25a59e00df29",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "a2b7cee7-2cec-48c4-90af-933e5e9fb6a8"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}