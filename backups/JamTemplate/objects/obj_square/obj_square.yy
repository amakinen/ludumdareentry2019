{
    "id": "d5e5ee86-e2a0-4cf1-8188-82fc8754bdcc",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_square",
    "eventList": [
        {
            "id": "4241c5db-712c-4b80-a1d8-089d774a9852",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "d5e5ee86-e2a0-4cf1-8188-82fc8754bdcc"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e5b3a08f-1d28-47d9-b2f6-a00a3e13c022",
    "visible": true
}