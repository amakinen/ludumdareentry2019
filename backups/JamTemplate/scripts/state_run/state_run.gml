/// @param _event

if (live_call(argument0)) return live_result;

var _event = argument0;

show_debug_message("State run: " + script_get_name(global.state) + " Create: " + string(global.state_create));
script_execute(global.state, global.state_create, _event);
