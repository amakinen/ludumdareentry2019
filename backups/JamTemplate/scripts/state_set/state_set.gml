/// @param _state
if (live_call(argument0)) return live_result;

var _state = argument0;
global.state_create = true;
global.state = _state;

show_debug_message("State set: " + script_get_name(_state));