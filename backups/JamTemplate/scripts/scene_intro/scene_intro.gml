/// @param create
/// @param event

if (live_call(argument0, argument1)) return live_result;

var create = argument0, event = argument1;



if (create) {
    alarm[0] = room_speed * 5;
    global.state_create = false;
}

if (event == "step") {
    if alarm[0] == -1 {
        state_set(scene_fade_in);
    }
}

if (event == "draw") {
    draw_set_alpha(1);
    draw_set_color(c_black);
    draw_rectangle(0,0, room_width, room_height, false);
    draw_set_color(c_white);
}