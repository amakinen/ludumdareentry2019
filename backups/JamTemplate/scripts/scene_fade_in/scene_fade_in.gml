/// @param create
/// @param event

if (live_call(argument0, argument1)) return live_result;

var create = argument0, event = argument1;



if (create) {
    show_debug_message("State create")
    fade_in_alpha = 1;
    global.state_create = false;
    with instance_create_depth(0,0,0, obj_inv) {
        item_list = [obj_bush1, obj_bush2];
    }
    
}

if (event == "step") {
    fade_in_alpha -= 0.01;
    if (fade_in_alpha < 0) fade_in_alpha = 0;
}


if (event == "draw") {
    draw_set_alpha(fade_in_alpha);
    draw_set_color(c_black);
    draw_rectangle(0,0, room_width, room_height, false);
    draw_set_color(c_white);
    draw_set_alpha(1);
}