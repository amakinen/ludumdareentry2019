{
    "id": "f6ef9f3f-8ea6-487b-b35d-120f24070217",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_square___copy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d47a4817-9b9a-4a7a-af4f-84cf28dd57c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f6ef9f3f-8ea6-487b-b35d-120f24070217",
            "compositeImage": {
                "id": "fab43ff7-0fd3-4640-9c8a-519e3a3a1424",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d47a4817-9b9a-4a7a-af4f-84cf28dd57c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e4cc6197-1c8d-4d45-9e0b-b5832656974d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d47a4817-9b9a-4a7a-af4f-84cf28dd57c0",
                    "LayerId": "3b32c4d1-8201-4a15-b114-51266a6a91a0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "3b32c4d1-8201-4a15-b114-51266a6a91a0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f6ef9f3f-8ea6-487b-b35d-120f24070217",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}