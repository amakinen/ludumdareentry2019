{
    "id": "058e94fc-ebb2-4197-a18f-3f2943006a45",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bush2_spawn",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7caf307d-318d-4dc6-b972-6389afe18395",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "058e94fc-ebb2-4197-a18f-3f2943006a45",
            "compositeImage": {
                "id": "6b99c952-caa2-46e8-8826-89fbed4b9969",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7caf307d-318d-4dc6-b972-6389afe18395",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c5045bf-ab24-4812-b287-72ded407d2a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7caf307d-318d-4dc6-b972-6389afe18395",
                    "LayerId": "911529a0-398e-4b00-837c-0d6146b6a5f3"
                }
            ]
        },
        {
            "id": "a49d4f45-f33e-43a1-9185-49850ad03b67",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "058e94fc-ebb2-4197-a18f-3f2943006a45",
            "compositeImage": {
                "id": "1eeefda8-a188-4910-b2c0-b4e1332df838",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a49d4f45-f33e-43a1-9185-49850ad03b67",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c449c59-4ad3-4998-ba16-a143fcd2094f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a49d4f45-f33e-43a1-9185-49850ad03b67",
                    "LayerId": "911529a0-398e-4b00-837c-0d6146b6a5f3"
                }
            ]
        },
        {
            "id": "895505eb-87f7-488e-906e-7a9928779507",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "058e94fc-ebb2-4197-a18f-3f2943006a45",
            "compositeImage": {
                "id": "e24c42dd-d31d-464a-ab33-25854bd2a2ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "895505eb-87f7-488e-906e-7a9928779507",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a8a9053-502a-4e52-813c-49544a21ed70",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "895505eb-87f7-488e-906e-7a9928779507",
                    "LayerId": "911529a0-398e-4b00-837c-0d6146b6a5f3"
                }
            ]
        },
        {
            "id": "60dddaf5-55b8-42d8-b0de-f676ac4d3f84",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "058e94fc-ebb2-4197-a18f-3f2943006a45",
            "compositeImage": {
                "id": "01bfc704-cda9-4e9a-ae58-3836334f65ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "60dddaf5-55b8-42d8-b0de-f676ac4d3f84",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae4de9cc-3e57-44fe-a644-8ec00d55ce0d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "60dddaf5-55b8-42d8-b0de-f676ac4d3f84",
                    "LayerId": "911529a0-398e-4b00-837c-0d6146b6a5f3"
                }
            ]
        },
        {
            "id": "99277fe2-ce25-4429-9a8c-25a76cfb7ee0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "058e94fc-ebb2-4197-a18f-3f2943006a45",
            "compositeImage": {
                "id": "1ae37d8f-b4d7-44c8-a132-65f05cf213cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "99277fe2-ce25-4429-9a8c-25a76cfb7ee0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e536dce-8c2e-4a43-8c71-58a336550d3f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99277fe2-ce25-4429-9a8c-25a76cfb7ee0",
                    "LayerId": "911529a0-398e-4b00-837c-0d6146b6a5f3"
                }
            ]
        },
        {
            "id": "0f97f9f1-7b69-48a3-a10b-90065439fadd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "058e94fc-ebb2-4197-a18f-3f2943006a45",
            "compositeImage": {
                "id": "822438ee-ee4a-4550-95db-e2efbbcce1fb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f97f9f1-7b69-48a3-a10b-90065439fadd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "00219fb0-bc7d-4568-a4c0-333a20f581d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f97f9f1-7b69-48a3-a10b-90065439fadd",
                    "LayerId": "911529a0-398e-4b00-837c-0d6146b6a5f3"
                }
            ]
        },
        {
            "id": "c1566926-55e7-4881-b10d-78181719ed25",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "058e94fc-ebb2-4197-a18f-3f2943006a45",
            "compositeImage": {
                "id": "371214cc-ecd4-4c4f-afb2-17b71eaf40bb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c1566926-55e7-4881-b10d-78181719ed25",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "32306c78-ef8a-4307-bfb8-f4d0d07c96e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c1566926-55e7-4881-b10d-78181719ed25",
                    "LayerId": "911529a0-398e-4b00-837c-0d6146b6a5f3"
                }
            ]
        },
        {
            "id": "99cecd8c-caff-42ba-9830-7a117ecdcd01",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "058e94fc-ebb2-4197-a18f-3f2943006a45",
            "compositeImage": {
                "id": "ea45dd5d-ab9f-48e2-bc71-4df29f24a64a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "99cecd8c-caff-42ba-9830-7a117ecdcd01",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "882cae2f-98e5-4ca3-be35-362ff15453ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99cecd8c-caff-42ba-9830-7a117ecdcd01",
                    "LayerId": "911529a0-398e-4b00-837c-0d6146b6a5f3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "911529a0-398e-4b00-837c-0d6146b6a5f3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "058e94fc-ebb2-4197-a18f-3f2943006a45",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}