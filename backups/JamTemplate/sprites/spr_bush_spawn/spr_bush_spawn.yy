{
    "id": "1e565043-3459-4235-9886-66f3acda49b0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bush_spawn",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a283cbc9-a464-451e-be63-49c70ed2475d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e565043-3459-4235-9886-66f3acda49b0",
            "compositeImage": {
                "id": "56d178dc-d2cd-47ac-811a-70630bc8c38e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a283cbc9-a464-451e-be63-49c70ed2475d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d47ba1be-1ef3-4f98-8397-bc3152531a62",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a283cbc9-a464-451e-be63-49c70ed2475d",
                    "LayerId": "b151387f-270f-44cf-998e-7e2f0510d776"
                }
            ]
        },
        {
            "id": "1f3a42cc-fb6e-4272-86fb-f95c8957679b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e565043-3459-4235-9886-66f3acda49b0",
            "compositeImage": {
                "id": "ef053829-0d59-4f3e-b853-accebf4de94a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f3a42cc-fb6e-4272-86fb-f95c8957679b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "906a65f7-7ffa-4069-b761-8d93e677cf1c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f3a42cc-fb6e-4272-86fb-f95c8957679b",
                    "LayerId": "b151387f-270f-44cf-998e-7e2f0510d776"
                }
            ]
        },
        {
            "id": "77f6756d-33a9-4f34-93c0-ac930ad91a70",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e565043-3459-4235-9886-66f3acda49b0",
            "compositeImage": {
                "id": "b3dafa23-57d2-4d90-a7ad-f87d2a5005c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77f6756d-33a9-4f34-93c0-ac930ad91a70",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc96d49c-3ad9-4527-8b01-d653556c5dbb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77f6756d-33a9-4f34-93c0-ac930ad91a70",
                    "LayerId": "b151387f-270f-44cf-998e-7e2f0510d776"
                }
            ]
        },
        {
            "id": "eb1ed41e-cbf9-40f9-b5f9-a78872bbf60a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e565043-3459-4235-9886-66f3acda49b0",
            "compositeImage": {
                "id": "4192529c-c4d4-4c26-86f1-9689f8bcfc5c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb1ed41e-cbf9-40f9-b5f9-a78872bbf60a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "09336172-3e58-4cdf-93d0-0dd9aba059f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb1ed41e-cbf9-40f9-b5f9-a78872bbf60a",
                    "LayerId": "b151387f-270f-44cf-998e-7e2f0510d776"
                }
            ]
        },
        {
            "id": "8ed22b32-bbdb-4426-b369-e80f7bbe586e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e565043-3459-4235-9886-66f3acda49b0",
            "compositeImage": {
                "id": "64f3b2f2-9c6e-4ede-bfff-49b68114e741",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ed22b32-bbdb-4426-b369-e80f7bbe586e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5bc782d-32ae-4067-8317-e9d712500526",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ed22b32-bbdb-4426-b369-e80f7bbe586e",
                    "LayerId": "b151387f-270f-44cf-998e-7e2f0510d776"
                }
            ]
        },
        {
            "id": "2a834299-0ba9-4056-8038-2d024c1b7fb4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e565043-3459-4235-9886-66f3acda49b0",
            "compositeImage": {
                "id": "89e6a8bb-08e2-4a03-bf71-4a81575f147c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a834299-0ba9-4056-8038-2d024c1b7fb4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "237b13f7-0cee-4452-a705-c00e031cdfed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a834299-0ba9-4056-8038-2d024c1b7fb4",
                    "LayerId": "b151387f-270f-44cf-998e-7e2f0510d776"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "b151387f-270f-44cf-998e-7e2f0510d776",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1e565043-3459-4235-9886-66f3acda49b0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}