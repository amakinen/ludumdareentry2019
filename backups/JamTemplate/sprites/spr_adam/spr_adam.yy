{
    "id": "06f4e7ba-d8bc-4f71-a4fa-0ae27ffa4138",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_adam",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9f8879d8-215c-42ab-a7b0-0ffba60eb217",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "06f4e7ba-d8bc-4f71-a4fa-0ae27ffa4138",
            "compositeImage": {
                "id": "dfa7528d-b65e-49f5-b658-06a959d23364",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f8879d8-215c-42ab-a7b0-0ffba60eb217",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f91b055-53a6-43fe-8fcc-52976671d78c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f8879d8-215c-42ab-a7b0-0ffba60eb217",
                    "LayerId": "6a185739-2c1d-4288-89ca-925d3ad0ec9e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "6a185739-2c1d-4288-89ca-925d3ad0ec9e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "06f4e7ba-d8bc-4f71-a4fa-0ae27ffa4138",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}