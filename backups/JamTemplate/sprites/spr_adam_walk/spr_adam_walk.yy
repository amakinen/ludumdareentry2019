{
    "id": "45ce6a91-9e2d-46fe-ab52-07b4e32dedbf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_adam_walk",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4ed99712-bf8b-4a4b-b0d8-7ed5f0523bef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "45ce6a91-9e2d-46fe-ab52-07b4e32dedbf",
            "compositeImage": {
                "id": "e6b876eb-c8aa-4991-b9cb-1d77717fe38d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4ed99712-bf8b-4a4b-b0d8-7ed5f0523bef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f5ccc6ad-f715-4474-af26-7689ac230080",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ed99712-bf8b-4a4b-b0d8-7ed5f0523bef",
                    "LayerId": "b5008c26-f44e-4023-816b-792be7bcc803"
                }
            ]
        },
        {
            "id": "a652b0bf-98cd-4f70-9704-1f5ebfb0b6db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "45ce6a91-9e2d-46fe-ab52-07b4e32dedbf",
            "compositeImage": {
                "id": "7c6b555c-8c2a-46c1-96bf-3f53996a9dae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a652b0bf-98cd-4f70-9704-1f5ebfb0b6db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e4b879ae-44fb-4760-bdaf-a9199e2ff2af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a652b0bf-98cd-4f70-9704-1f5ebfb0b6db",
                    "LayerId": "b5008c26-f44e-4023-816b-792be7bcc803"
                }
            ]
        },
        {
            "id": "689ad1f1-c449-46b8-9a61-0ee96b85d589",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "45ce6a91-9e2d-46fe-ab52-07b4e32dedbf",
            "compositeImage": {
                "id": "412d9beb-321d-453e-a72f-131966c1b50c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "689ad1f1-c449-46b8-9a61-0ee96b85d589",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "65dfc330-8939-47ef-9ec8-e45be6bbe842",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "689ad1f1-c449-46b8-9a61-0ee96b85d589",
                    "LayerId": "b5008c26-f44e-4023-816b-792be7bcc803"
                }
            ]
        },
        {
            "id": "46294ebb-4bba-4fbb-bfb4-10e1d7bbdae1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "45ce6a91-9e2d-46fe-ab52-07b4e32dedbf",
            "compositeImage": {
                "id": "3de512e7-870a-495e-af2a-b7b4107dfc20",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46294ebb-4bba-4fbb-bfb4-10e1d7bbdae1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "93863bbb-f9fa-4c0d-8180-e96229400cd1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46294ebb-4bba-4fbb-bfb4-10e1d7bbdae1",
                    "LayerId": "b5008c26-f44e-4023-816b-792be7bcc803"
                }
            ]
        },
        {
            "id": "629d9dbc-ac2b-4628-9430-83e6230ea4a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "45ce6a91-9e2d-46fe-ab52-07b4e32dedbf",
            "compositeImage": {
                "id": "df2115aa-c73c-463b-9878-c21732932d8e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "629d9dbc-ac2b-4628-9430-83e6230ea4a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed3cb2da-95e3-4709-85ea-f2a0f2ef762f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "629d9dbc-ac2b-4628-9430-83e6230ea4a7",
                    "LayerId": "b5008c26-f44e-4023-816b-792be7bcc803"
                }
            ]
        },
        {
            "id": "ebdd4ae4-99ae-41bb-8e80-f371507597b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "45ce6a91-9e2d-46fe-ab52-07b4e32dedbf",
            "compositeImage": {
                "id": "47f215ff-7bf3-4869-bc94-bbead1432b6e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ebdd4ae4-99ae-41bb-8e80-f371507597b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "44f94384-59e4-4724-8edf-fbf047b4d6cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ebdd4ae4-99ae-41bb-8e80-f371507597b8",
                    "LayerId": "b5008c26-f44e-4023-816b-792be7bcc803"
                }
            ]
        },
        {
            "id": "a30ba14a-6f8f-41c2-9838-47dfdb06bed2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "45ce6a91-9e2d-46fe-ab52-07b4e32dedbf",
            "compositeImage": {
                "id": "bf1b44c4-586c-4304-b341-3e4178aaed89",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a30ba14a-6f8f-41c2-9838-47dfdb06bed2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8cea6f08-b02c-41fb-9ed5-c2145ee2e7e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a30ba14a-6f8f-41c2-9838-47dfdb06bed2",
                    "LayerId": "b5008c26-f44e-4023-816b-792be7bcc803"
                }
            ]
        },
        {
            "id": "648bd9ad-9105-420a-89c3-01c86ef8ec87",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "45ce6a91-9e2d-46fe-ab52-07b4e32dedbf",
            "compositeImage": {
                "id": "437d9794-1f4a-43a7-8275-b5a8d1689ba2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "648bd9ad-9105-420a-89c3-01c86ef8ec87",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "31c5f20c-381b-4096-a191-0a5f8c1b6e72",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "648bd9ad-9105-420a-89c3-01c86ef8ec87",
                    "LayerId": "b5008c26-f44e-4023-816b-792be7bcc803"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "b5008c26-f44e-4023-816b-792be7bcc803",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "45ce6a91-9e2d-46fe-ab52-07b4e32dedbf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}