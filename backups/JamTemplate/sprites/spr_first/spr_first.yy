{
    "id": "503c3778-0d77-441e-aa0b-cd037445e8b5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_first",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d5a67669-8b2c-4e9e-b506-2885c8ea0e69",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "503c3778-0d77-441e-aa0b-cd037445e8b5",
            "compositeImage": {
                "id": "37c4794b-08c8-4df8-92a7-42c6c119b829",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d5a67669-8b2c-4e9e-b506-2885c8ea0e69",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1456dc26-8f30-4840-8e1f-7d9efaa4eb77",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d5a67669-8b2c-4e9e-b506-2885c8ea0e69",
                    "LayerId": "30174bc1-ff99-46a5-90d0-1bf696fcf0d6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "30174bc1-ff99-46a5-90d0-1bf696fcf0d6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "503c3778-0d77-441e-aa0b-cd037445e8b5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}