{
    "id": "e5b3a08f-1d28-47d9-b2f6-a00a3e13c022",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_square",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "94717ef8-a8b0-4f74-a4a6-c0986f56f39e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5b3a08f-1d28-47d9-b2f6-a00a3e13c022",
            "compositeImage": {
                "id": "db999ade-917b-4652-887d-0f1f0a3543b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "94717ef8-a8b0-4f74-a4a6-c0986f56f39e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8aa5dec-087d-4fa6-8d66-5496a7192381",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "94717ef8-a8b0-4f74-a4a6-c0986f56f39e",
                    "LayerId": "5fa635e7-0b95-4e14-a851-4a81c72cf331"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "5fa635e7-0b95-4e14-a851-4a81c72cf331",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e5b3a08f-1d28-47d9-b2f6-a00a3e13c022",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}