{
    "id": "8dec0db0-5388-4c71-ab37-dae94a997ad2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bush1_spawn",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "eb40d2e4-e104-439a-a2d6-d8d21f9cd08e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8dec0db0-5388-4c71-ab37-dae94a997ad2",
            "compositeImage": {
                "id": "bdc58a76-1c01-454c-b1c4-15a7c58547bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb40d2e4-e104-439a-a2d6-d8d21f9cd08e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eda7ce21-f792-4594-85d8-39bc1eac0a3e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb40d2e4-e104-439a-a2d6-d8d21f9cd08e",
                    "LayerId": "445da78e-d228-4b08-b947-45ef1507a7c2"
                }
            ]
        },
        {
            "id": "50c4fb95-50f2-47af-8396-ad1edd083cad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8dec0db0-5388-4c71-ab37-dae94a997ad2",
            "compositeImage": {
                "id": "004dbb3e-3cf3-4e48-a9f2-f2447532133f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "50c4fb95-50f2-47af-8396-ad1edd083cad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ebf998e-b5e8-4f4a-baa8-c791b9cbb3fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "50c4fb95-50f2-47af-8396-ad1edd083cad",
                    "LayerId": "445da78e-d228-4b08-b947-45ef1507a7c2"
                }
            ]
        },
        {
            "id": "3caf8521-f3fc-45ba-b943-7a606b8aa151",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8dec0db0-5388-4c71-ab37-dae94a997ad2",
            "compositeImage": {
                "id": "9baa5e4e-a878-404c-aaa9-b423683f1c7c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3caf8521-f3fc-45ba-b943-7a606b8aa151",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a128a94-2fdf-439a-ae04-7b854612e3eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3caf8521-f3fc-45ba-b943-7a606b8aa151",
                    "LayerId": "445da78e-d228-4b08-b947-45ef1507a7c2"
                }
            ]
        },
        {
            "id": "38ea2f5e-ed58-4537-900e-fa15e2e06eba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8dec0db0-5388-4c71-ab37-dae94a997ad2",
            "compositeImage": {
                "id": "4db58a41-7d05-4a56-b55c-aa55d01f4eeb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "38ea2f5e-ed58-4537-900e-fa15e2e06eba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "716a0f16-e0fa-4394-9a6f-8eeacc3bb753",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "38ea2f5e-ed58-4537-900e-fa15e2e06eba",
                    "LayerId": "445da78e-d228-4b08-b947-45ef1507a7c2"
                }
            ]
        },
        {
            "id": "b2c0b7f5-2199-4d2e-a143-9ac4c63cff4f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8dec0db0-5388-4c71-ab37-dae94a997ad2",
            "compositeImage": {
                "id": "6d57a3c1-78af-47af-b5eb-e745350d048b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b2c0b7f5-2199-4d2e-a143-9ac4c63cff4f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "021883e7-bc26-44ae-a031-ee2745288dbf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b2c0b7f5-2199-4d2e-a143-9ac4c63cff4f",
                    "LayerId": "445da78e-d228-4b08-b947-45ef1507a7c2"
                }
            ]
        },
        {
            "id": "ff5a49ad-a250-424c-b08d-e27714233b17",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8dec0db0-5388-4c71-ab37-dae94a997ad2",
            "compositeImage": {
                "id": "0ccea957-86c3-415d-ae43-e6b963425f96",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff5a49ad-a250-424c-b08d-e27714233b17",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1522cb26-ca68-4214-afb5-bfe8348ffee1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff5a49ad-a250-424c-b08d-e27714233b17",
                    "LayerId": "445da78e-d228-4b08-b947-45ef1507a7c2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "445da78e-d228-4b08-b947-45ef1507a7c2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8dec0db0-5388-4c71-ab37-dae94a997ad2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}