{
    "id": "c2321594-5c62-487f-9abe-5af9434d1962",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bush1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "54e6215d-dcdd-46fd-ad09-da4d45776f9c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c2321594-5c62-487f-9abe-5af9434d1962",
            "compositeImage": {
                "id": "ebde6020-c853-43a3-a554-9ab17f63e0b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "54e6215d-dcdd-46fd-ad09-da4d45776f9c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c880eed7-3fc5-4d3e-b30a-9aece6ed0e37",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54e6215d-dcdd-46fd-ad09-da4d45776f9c",
                    "LayerId": "3ee9cefa-4dca-4fea-b047-d62565070c23"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "3ee9cefa-4dca-4fea-b047-d62565070c23",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c2321594-5c62-487f-9abe-5af9434d1962",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}