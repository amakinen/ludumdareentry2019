{
    "id": "68b75255-ca29-4150-9248-c04c74f2eb93",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_square_two",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2317b173-7f0a-45df-8455-36ecbf8a15bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "68b75255-ca29-4150-9248-c04c74f2eb93",
            "compositeImage": {
                "id": "93c43bf2-7d6d-4233-8e15-cb6a1d4d5b68",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2317b173-7f0a-45df-8455-36ecbf8a15bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f9878a3-aa39-41be-a478-38c162af03ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2317b173-7f0a-45df-8455-36ecbf8a15bb",
                    "LayerId": "1410dde9-a5c8-4a3f-bd5b-9aed13c82878"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "1410dde9-a5c8-4a3f-bd5b-9aed13c82878",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "68b75255-ca29-4150-9248-c04c74f2eb93",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}