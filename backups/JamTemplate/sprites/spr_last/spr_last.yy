{
    "id": "2f3677fc-213a-4391-92e8-09d615bb9874",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_last",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "402e6881-2b26-406e-b94b-1c5999330e74",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2f3677fc-213a-4391-92e8-09d615bb9874",
            "compositeImage": {
                "id": "baff3273-0f37-4903-9d0e-875b2ea48093",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "402e6881-2b26-406e-b94b-1c5999330e74",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd6625d2-30a6-43e9-be5c-2f8fd5bd73e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "402e6881-2b26-406e-b94b-1c5999330e74",
                    "LayerId": "6cb8c9f8-1471-452d-a017-4b969949d54c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "6cb8c9f8-1471-452d-a017-4b969949d54c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2f3677fc-213a-4391-92e8-09d615bb9874",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}