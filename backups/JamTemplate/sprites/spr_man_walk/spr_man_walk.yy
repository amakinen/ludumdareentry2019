{
    "id": "2426cff2-4b08-4aa8-bdfe-e698fef6c67c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_man_walk",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5aa922be-74bc-46ca-a5c2-592062ac9379",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2426cff2-4b08-4aa8-bdfe-e698fef6c67c",
            "compositeImage": {
                "id": "aacf52bf-ca01-40be-a698-065981398adb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5aa922be-74bc-46ca-a5c2-592062ac9379",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bda427ec-c209-4a68-a2ec-e8a8bc4fab38",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5aa922be-74bc-46ca-a5c2-592062ac9379",
                    "LayerId": "e86a4350-792a-4c9e-9873-892eef81a456"
                }
            ]
        },
        {
            "id": "43b80457-dd9c-4c3c-9cc2-b06d0fae9f3b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2426cff2-4b08-4aa8-bdfe-e698fef6c67c",
            "compositeImage": {
                "id": "6b5c211e-c479-4b8a-b420-96db95b5ccc7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "43b80457-dd9c-4c3c-9cc2-b06d0fae9f3b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b463bef-e065-4511-abf8-54f49ce32006",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43b80457-dd9c-4c3c-9cc2-b06d0fae9f3b",
                    "LayerId": "e86a4350-792a-4c9e-9873-892eef81a456"
                }
            ]
        },
        {
            "id": "e36f7dbd-8945-40aa-8e9c-0512769cd23d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2426cff2-4b08-4aa8-bdfe-e698fef6c67c",
            "compositeImage": {
                "id": "62a43a95-502f-471e-b208-9ec829df2a90",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e36f7dbd-8945-40aa-8e9c-0512769cd23d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4babbaac-dad0-4ca8-ba43-cd709864794c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e36f7dbd-8945-40aa-8e9c-0512769cd23d",
                    "LayerId": "e86a4350-792a-4c9e-9873-892eef81a456"
                }
            ]
        },
        {
            "id": "8f12e223-1f56-48c8-aeb1-d9f40a76e8fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2426cff2-4b08-4aa8-bdfe-e698fef6c67c",
            "compositeImage": {
                "id": "1d8fac93-9a1d-4e1b-aa6d-e2e1f4535f9b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f12e223-1f56-48c8-aeb1-d9f40a76e8fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f9fab044-d04b-4152-916f-b0bf469bf7e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f12e223-1f56-48c8-aeb1-d9f40a76e8fc",
                    "LayerId": "e86a4350-792a-4c9e-9873-892eef81a456"
                }
            ]
        },
        {
            "id": "d06d00c4-3def-4e89-8253-4009a887a8db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2426cff2-4b08-4aa8-bdfe-e698fef6c67c",
            "compositeImage": {
                "id": "1b6a5891-3770-4bb9-b4c3-7f917b4351c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d06d00c4-3def-4e89-8253-4009a887a8db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c05acd01-5f28-449d-bca9-927c2b7b5d47",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d06d00c4-3def-4e89-8253-4009a887a8db",
                    "LayerId": "e86a4350-792a-4c9e-9873-892eef81a456"
                }
            ]
        },
        {
            "id": "e663bec4-22d3-4071-8749-f29736cc9c09",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2426cff2-4b08-4aa8-bdfe-e698fef6c67c",
            "compositeImage": {
                "id": "21394fd5-d00b-40a2-8163-2d19fe021d9f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e663bec4-22d3-4071-8749-f29736cc9c09",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "950e43d6-3fed-42e7-a9c3-a3077132972c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e663bec4-22d3-4071-8749-f29736cc9c09",
                    "LayerId": "e86a4350-792a-4c9e-9873-892eef81a456"
                }
            ]
        },
        {
            "id": "48667075-46a1-4850-aab5-0bd1f7091541",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2426cff2-4b08-4aa8-bdfe-e698fef6c67c",
            "compositeImage": {
                "id": "60919324-42a7-4ce7-a25d-f9c39c9a82c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "48667075-46a1-4850-aab5-0bd1f7091541",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "80f4b2c5-b8c8-44fe-b9b4-e02829a4e5b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "48667075-46a1-4850-aab5-0bd1f7091541",
                    "LayerId": "e86a4350-792a-4c9e-9873-892eef81a456"
                }
            ]
        },
        {
            "id": "563a2715-6038-4359-b203-1ff7f5f4c263",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2426cff2-4b08-4aa8-bdfe-e698fef6c67c",
            "compositeImage": {
                "id": "587827ed-64c9-495e-8fac-75e48605eca4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "563a2715-6038-4359-b203-1ff7f5f4c263",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1593ea59-0be7-44c4-858e-5e81132d882d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "563a2715-6038-4359-b203-1ff7f5f4c263",
                    "LayerId": "e86a4350-792a-4c9e-9873-892eef81a456"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "e86a4350-792a-4c9e-9873-892eef81a456",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2426cff2-4b08-4aa8-bdfe-e698fef6c67c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}