{
    "id": "19738fff-1582-47c3-a725-c9297e368ea9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bush2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6ade03ae-88a3-4a4c-9e04-09248e2ab253",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19738fff-1582-47c3-a725-c9297e368ea9",
            "compositeImage": {
                "id": "adddc85e-c2d9-461c-b702-b353e40a395a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ade03ae-88a3-4a4c-9e04-09248e2ab253",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6821e9bf-00f1-4973-927e-8c557473bc61",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ade03ae-88a3-4a4c-9e04-09248e2ab253",
                    "LayerId": "cea3177f-664d-4d5f-98b6-4f7c11e5a637"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "cea3177f-664d-4d5f-98b6-4f7c11e5a637",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "19738fff-1582-47c3-a725-c9297e368ea9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}