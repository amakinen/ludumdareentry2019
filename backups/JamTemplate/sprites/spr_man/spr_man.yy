{
    "id": "986f4193-4358-45d9-83d5-6e2ee1b4d461",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_man",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8ab80598-85f5-4bc6-94c8-5221c7e5fb24",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "986f4193-4358-45d9-83d5-6e2ee1b4d461",
            "compositeImage": {
                "id": "6209af60-99cf-4c27-be71-8c7ecfb51fe2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ab80598-85f5-4bc6-94c8-5221c7e5fb24",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5d6ab563-10e0-4f71-974f-716b7a0cd40c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ab80598-85f5-4bc6-94c8-5221c7e5fb24",
                    "LayerId": "95aa9fbe-e0d1-4905-bf5d-b0b500e618a7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "95aa9fbe-e0d1-4905-bf5d-b0b500e618a7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "986f4193-4358-45d9-83d5-6e2ee1b4d461",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}