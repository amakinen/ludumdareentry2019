{
    "id": "b98364b5-221f-4f6f-a7d9-7fb852b24f8a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "mieka",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a320f9af-248f-41da-8c38-02cc9990973d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b98364b5-221f-4f6f-a7d9-7fb852b24f8a",
            "compositeImage": {
                "id": "c07ccf63-42b9-4d06-864c-cd6e655c9066",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a320f9af-248f-41da-8c38-02cc9990973d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e04381b-52b7-4450-9374-f16de645ca8c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a320f9af-248f-41da-8c38-02cc9990973d",
                    "LayerId": "daef167c-797f-4634-b9f5-2606680e0c31"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "daef167c-797f-4634-b9f5-2606680e0c31",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b98364b5-221f-4f6f-a7d9-7fb852b24f8a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}